from matrix_client.client import MatrixClient
import matrix_client
import argparse

# Parsing arguments from the command line
parser = argparse.ArgumentParser()
parser.add_argument("--server", help="Server of your matrix account")
parser.add_argument("--username", help="Your bot username for clean up")
parser.add_argument("--password", help="Password for the user/bot")
parser.add_argument("--room", help="Select room id for cleaning")
args = parser.parse_args()

# Creating Matrix Client
client = MatrixClient(args.server, sync_filter_limit=1000)

#Login
print("LOGIN")
token = client.login(username=args.username, password=args.password, limit=1000, sync=True)
print("LOGGED IN")

# Working with the room
room = client.join_room(args.room)
print("JOINED THE ROOM")
room.event_history_limit = 1000
room.backfill_previous_messages(limit=1000)
events = room.get_events()
for event in events:
    if event['type'] == 'm.room.encrypted':
        print("Deleting message {} message".format(event['event_id']))
        room.redact_message(event['event_id'])
