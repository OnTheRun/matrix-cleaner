# Matrix Cleaner
Small project to clean up messages from matrix channel in bulk

## Running the script
Just run from the command line after installing all dependencies with specified flags as follows

<code>python3 matrix_cleaner.py --server --username --passwod --room</code>

Username should be written in matrix format @username:example.com

For room ID use internal room ID: in element you can find it in Room Settings -> Advanced -> Internal Room ID

## Dependencies
matrix-client
